function frames = read(varargin)
%READ Write yuv file from a cell array of frames
%   READ(frames,'name',name) read yuv file with corresponding to name filename
%   READ(frames,'recDir',recDir) read from directory recDir
%   READ(frames,'subSamp',subSamp) specify YUV sampling scheme
%   READ(frames,'writePrecision',writePrecision) specify write data type
%   READ(frames,'readPrecision',readPrecision) specify read data type

%% Parse input parameters
filename = fullfile(pwd,'sequence');

p = inputParser; p.StructExpand = true; p.KeepUnmatched = true;
p.addParameter('filename', filename, @ischar);
p.addParameter('addInfo' , true    , @islogical);

p.parse(varargin{:});

filename = p.Results.filename;
addInfo  = p.Results.addInfo;

if addInfo
    [~,imgSize,imgRes,subSamp,bitDepth] = yuv.name2params(filename);
else
    imgSize = [0,0,0];
    imgRes = [0,0];
    bitDepth = 0;
    
    p.addParameter('imgSize', imgSize, @isnumeric);
    p.parse(varargin{:});
    
    imgSize = p.Results.imgSize;
    
    switch imgSize(3)
        case 1
            subSamp = '400';
        case 3
            subSamp = '444';
    end
    
    p.addParameter('imgRes'  , imgRes  , @isnumeric);
    p.addParameter('subSamp' , subSamp , @ischar);
    p.addParameter('bitDepth', bitDepth, @isnumeric);
    
    p.parse(varargin{:});
    
    imgRes   = p.Results.imgRes;
    subSamp  = p.Results.subSamp;
    bitDepth = p.Results.bitDepth;
end

p.addParameter( 'inColSpace', 'rgb'  , @ischar);
p.addParameter('outColSpace', 'ycbcr', @ischar);

p.parse(varargin{:});

inColSpace  = p.Results.inColSpace;
outColSpace = p.Results.outColSpace;

precision = ['uint',num2str(max(8,2^ceil(log2(bitDepth))))];

%% Incompatibility

if imgSize(3)~=1 && imgSize(3)~=3
    error('Unexpected number of channels');
end

if imgSize(3)==1 && ~strcmp(subSamp,'400')
    error('Incompatible size and subsampling');
end

if imgSize(3)==3 &&  strcmp(subSamp,'400')
    error('Incompatible size and subsampling');
end

%% YUV 400/420/422: define interpolation sampling grids
[xq,xgv] = deal(1:imgSize(1));
[yq,ygv] = deal(1:imgSize(2));

switch subSamp
    case '422'
        ygv = 1:2:imgSize(2);
    case '420'
        xgv = 1.5:2:imgSize(1); ygv = 1:2:imgSize(2);
end

subImgSize = [numel(xgv),numel(ygv)];
UV = griddedInterpolant({xgv,ygv},zeros(subImgSize),'linear','nearest');

%% Read frames iteratively
%filename = yuv.params2name(filename,imgSize,imgRes,subSamp,bitDepth);

fileID = fopen(filename,'r');

imgRes(end+1:2) = 1;
imgGrid = zeros(imgRes);
if strcmp(subSamp,'400')
    frames = arrayfun(@readY,imgGrid,'UniformOutput',false);
else
    frames = arrayfun(@readYUV,imgGrid,'UniformOutput',false);
end

fclose(fileID);

%% Perform color space transform
if ~strcmp(subSamp,'400')
    frames = yuv.convert(frames,inColSpace,outColSpace,bitDepth);
end

%% Reshape frames
frames = reshape(frames,imgRes);

%% Auxiliary function
% Read a single frame of given dimensions
    function frame = readFrame(imgSize)
        frame = fread(fileID,flip(imgSize),['*',precision])';
    end

% Read a single Y frame
    function Y = readY(~)
        Y = readFrame(imgSize(1:2));
    end

%Read Y, U and V frames
    function YUV = readYUV(~)
        Y = readFrame(imgSize(1:2));
        UV.Values = double(readFrame(subImgSize));
        U = cast(UV({xq,yq}),precision);
        UV.Values = double(readFrame(subImgSize));
        V = cast(UV({xq,yq}),precision);
        YUV = cat(3,Y,U,V);
    end
end