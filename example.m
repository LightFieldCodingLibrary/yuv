clear all, close all, clc
%%
subSamp ='444';
colSpace = 'yuv';
name = 'test';
precision = 'uint8';

%%
load('C:\Users\edib\Light Fields\Heidelberg\full_data\test\bedroom\LF.mat');
LF = LF.LF;
% LF = LF(1:5,3:4,34:307,88:400,:);
% LF = double(uint8(255*LF))/255;
% LF = zeros(size(LF),'double');
s = size(LF);
frames = mat2cell(LF,ones(1,s(1)),ones(1,s(2)),s(3),s(4),s(5));

%%
convFrames = yuv.convert(frames,...
    'subSamp',subSamp,...
    'colSpace',colSpace);

%%
[imgRes,imgSize,convFrames] = yuv.write(frames,name,...
    'colSpace',colSpace,...
    'subSamp',subSamp,...
    'precision',precision);

%%
convFrames_ = yuv.read(name,imgRes,imgSize,...
        'colSpace',colSpace,...
    'subSamp',subSamp,...
    'precision',precision);

%%
close all
i=1;
figure, imshow(convFrames{1,1}{i})
figure, imshow(convFrames_{1,1}{i})
figure, imshow(abs(convFrames{1,1}{i}-convFrames{1,1}{i}),[])



